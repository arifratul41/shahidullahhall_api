Django==3.0.2
python-dotenv==0.10.3
mysqlclient==1.4.5
django-compressor==2.4
requests==2.22.0
pandas==0.25.3
djangorestframework==3.11.0
xlrd==1.2.0
