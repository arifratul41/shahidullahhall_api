from django.apps import AppConfig


class IntegrationServiceConfig(AppConfig):
    name = 'integration_service'
