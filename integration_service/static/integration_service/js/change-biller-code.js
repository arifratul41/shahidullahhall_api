/**
 * Created by nayan on 10/10/17.
 */
var MyDateField = function (config) {
    jsGrid.Field.call(this, config);
};

MyDateField.prototype = new jsGrid.Field({
    editTemplate: function (value) {
        return this._editPicker = $("<input>").datetimepicker({format: 'YYYY-MM-DD'});
    },

    editValue: function () {
        return this._editPicker.val();
    }
});

jsGrid.fields.date = MyDateField;

var designations = [
    {Name: "Head Teacher", Id: 1}, {Name: "Head Teacher (Acting)", Id: 3}, {Name: "Super", Id: 4}
];

function editTemplate(value, item) {
    var container = $('<div></div>');
    this._txtInput = $("<input>").val(value);
    container.append(this._txtInput);
    container.append($('<span></span>').addClass('text-danger small'));
    return container;
}

function editValue() {
    return this._txtInput.val();
}


function getFields(unions) {
    unions.unshift({id: '', name: ''});

    return [
        {name: "school_code", type: "text", editing: false, title: "School Code"},
        {name: "school_name", type: "text", editing: false, title: "School Name"},
        {
            name: "name", type: "text", validate: "required", title: "Teacher Name",
            editcss: 'name-edit', editTemplate: editTemplate, editValue: editValue
        },
        {
            name: "mobile", "title": "Mobile", type: "text", editcss: 'mobile-edit',
            validate: ["required", {
                validator: "pattern",
                param: "^01[3456789][0-9]{8}$",
                "message": "Invalid mobile number"
            }],
            editTemplate: editTemplate, editValue: editValue
        },
        {
            name: "nid", type: "text", title: "NID", sorting: false, "filtering": false, editcss: 'nid-edit',
            validate: [{
                validator: "pattern",
                param: "^$|[0-9]{10}$|^[0-9]{13}$|^[0-9]{17}$",
                "message": "Must be a 10, 13 or digit number"
            }],
            editTemplate: editTemplate, editValue: editValue
        },
        {
            name: "date_of_birth", type: "date", title: "Date of Birth",
            sorting: false, filtering: false, css: 'date-field'
        },
        {
            name: "designation", type: "select", title: "Designation", sorting: false, items: designations,
            valueField: "Id", textField: "Name", filtering: false, editcss: 'designation-edit'
        },
        {
            name: "union", type: "select", title: "Union", sorting: false, items: unions,
            valueField: "id", textField: "name", filtering: false
        },
        {type: "control", deleteButton: false, clearFilterButton: true}
    ];
}

function loadGrid(unions, teacher_list) {
    $("#jsGrid").jsGrid({
        width: "100%",
        inserting: false,
        editing: CAN_CHANGE,
        sorting: true,
        paging: false,
        autoload: true,
        filtering: true,
        fields: getFields(unions),
        invalidNotify: function (obj) {
            $('.jsgrid-edit-row span.text-danger').text('');
            for (var i = 0, size = obj.errors.length; i < size; i++) {
                var fieldSelector = ('.' + obj.errors[i].field.editcss + ' span.text-danger');
                if (!$(fieldSelector).text()) {
                    $(fieldSelector).text(obj.errors[i].message);
                }
            }
        },
        rowClick: function (args) {
            var $row = $(args.event.target).closest("tr");
            if (this._editingRow) {
                var updateItemPromise = this.updateItem();
                if (updateItemPromise) {
                    updateItemPromise.done($.proxy(function () {
                        this.editing && this.editItem($row);
                    }, this));
                }
                return;
            }

            this.editing && this.editItem($row);
        },
        controller: {
            loadData: function (filter) {
                return $.grep(teacher_list, function (item) {
                    return (
                        (!filter.school_code || item.school_code.toString().indexOf(filter.school_code) >= 0)
                        &&
                        (!filter.school_name || item.school_name.toUpperCase().indexOf(filter.school_name.toUpperCase()) >= 0)
                        &&
                        (!filter.name || item.name.toUpperCase().indexOf(filter.name.toUpperCase()) >= 0)
                        &&
                        (!filter.mobile || (item.mobile && item.mobile.indexOf(filter.mobile) >= 0))
                    );
                });
            },
            updateItem: function (item) {

                $('.jsgrid-edit-row span.text-danger').text('');

                var d = $.Deferred();
                $.ajax({
                    type: "PUT",
                    url: SAVE_TEACHER_API_URL.replace('/0/save', '/' + item.id + '/save'),
                    dataType: "json",
                    data: item
                }).done(function (response) {
                    d.resolve(item);
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    var errorJson = jqXHR.responseJSON;
                    if (errorJson['nid']) {
                        $('.nid-edit span.text-danger').text(errorJson['nid'].join(", "));
                    }
                    if (errorJson['mobile']) {
                        $('.mobile-edit span.text-danger').text(errorJson['mobile'].join(", "));
                    }
                    if (errorJson['name']) {
                        $('.name-edit span.text-danger').text(errorJson['name'].join(", "));
                    }
                    if (errorJson['designation']) {
                        $('.designation-edit span.text-danger').text(errorJson['designation'].join(", "));
                    }
                    d.reject(item);
                });

                return d.promise();
            }
        }
    });
}


function validate_mobile_or_wallet(value) {
    var mobile_regex = new RegExp('^01[3-9][0-9]{8}$');
    var wallet_regex = new RegExp('^01[3-9][0-9]{9}$');

    if (value) {
        value = value.trim();
    } else {
        return false;
    }

    return mobile_regex.test(value) || wallet_regex.test(value);
}

$(document).ready(function () {
    $("#id_search").click(function () {
        var mobile_or_wallet = $('#id_mobile_or_wallet').val();

        if (!validate_mobile_or_wallet(mobile_or_wallet)) {
            $('#id_error_div').text("Please enter a valid mobile or wallet.");
        } else {
            $('#id_error_div').text("");

            $.ajax({
                url: GET_API_URL + "?mobile_or_wallet=" + mobile_or_wallet.trim(),
                type: 'GET',
                dataType: "json"
            }).done(function (response) {
                loadGrid(response.data);
                $('#id_data_count').html(response.data.length + " data found").show();
            });
        }
    });

});