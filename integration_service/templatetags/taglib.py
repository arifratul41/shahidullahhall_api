from django.urls import resolve
from django.template import Library
import calendar
from django.contrib.auth.models import Group
from django.conf import settings

register = Library()

@register.filter
def percentage(value):
    return format(value, "%")

@register.simple_tag
def nav_active(request, view_name):
    """
    In template: {% nav_active request "url_name_here" %}
    """
    url_name = resolve(request.path).url_name
    if url_name == view_name:
        return "active"
    return ""


@register.simple_tag
def active_url_matches(request, url_part, position=1):
    """
    Returns "active" if url part matches with provided name. By default it'll match with first part. 
    To match with different part provide the position as last parameter
    Sample:
    if url is /home/user/profile,
    {% active_url_matches request "home" %} will print active
    """
    url_name = request.path.split('/')[position]
    if url_name == url_part:
        return "active"
    return ""


@register.simple_tag
def class_eq(class_name, var1, var2):
    """
    Return the class_name if var1 equals var2
    :param class_name: 
    :param var1: 
    :param var2: 
    :return: 
    """
    if var1 == var2:
        return class_name
    return ""


@register.filter
def month_name(month_number):
    return calendar.month_name[month_number]


@register.filter
def has_group(user, group_name):
    group = Group.objects.get(name=group_name)
    return True if group in user.groups.all() else False
