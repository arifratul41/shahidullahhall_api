from django.conf.urls import url

from integration_service.views import home

urlpatterns = [
    url(r'^$', home.home, name='home'),
]
